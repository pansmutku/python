import turtle # Importujemy turtle`a


# Okno
okno = turtle.Screen() # Okienko
okno.title("Serce by pansmutku") # Tytul
okno.bgcolor("black") # Kolor tla
okno.setup(width=500, height=600) # Rozdzielczosc
okno.tracer(0) # Update
turtle.pensize(4)
def serce():
    for i in range(200):
        turtle.right(1)
        turtle.forward(1)

turtle.speed(0)
turtle.color("red")

turtle.begin_fill()
turtle.left(140)
turtle.forward(111.65)
serce()

turtle.left(120)
serce()
turtle.forward(111.65)
turtle.end_fill()
turtle.hideturtle()

# Podpis
podpis = turtle.Turtle() # Definiujemy podpis
podpis.speed(0) # Animation speed  / Szybkość animacji
podpis.color("white") # Kolor
podpis.penup() # Narysuj sie
podpis.hideturtle() # Ukryj sie
podpis.goto(0, 210) # Pozycja
podpis.write("Serce by pansmutku", align="center", font=("Courier", 24, "normal") ) # Wystylizowany napis

while True: # Petla nieskonczona
    okno.update() # Aktualizacja okna