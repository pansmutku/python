import turtle # Importujemy turtle`a


# Okno
okno = turtle.Screen() # Okienko
okno.title("Kola by pansmutku") # Tytul
okno.bgcolor("black") # Kolor tla
okno.setup(width=700, height=600) # Rozdzielczosc
okno.tracer(0) # Update

# kolo
kolo = turtle.Turtle() # Definiujemy kwadratmaly
kolo.speed(0) # Szybkosc animacji
kolo.goto(0, 0) # Pozycja
kolo.color("white") # Kolor
kolo.circle(100) # Tworzymy kolo
kolo.penup() # Narysuj sie

# kolorowe kola

# niebieskie kolo
kolo = turtle.Turtle() # Definiujemy kwadratmaly
kolo.speed(0) # Szybkosc animacji
kolo.goto(-200, -200) # Pozycja
kolo.color("blue") # Kolor
kolo.circle(80) # Tworzymy kolo
kolo.penup() # Narysuj sie

# czerwone kolo
kolo = turtle.Turtle() # Definiujemy kwadratmaly
kolo.speed(0) # Szybkosc animacji
kolo.goto(0, -200) # Pozycja
kolo.color("red") # Kolor
kolo.circle(80) # Tworzymy kolo
kolo.penup() # Narysuj sie

# zolte kolo
kolo = turtle.Turtle() # Definiujemy kwadratmaly
kolo.speed(0) # Szybkosc animacji
kolo.goto(200, -200) # Pozycja
kolo.color("yellow") # Kolor
kolo.circle(80) # Tworzymy kolo
kolo.penup() # Narysuj sie

# Podpis
podpis = turtle.Turtle() # Definiujemy podpis
podpis.speed(0) # Animation speed  / Szybkość animacji
podpis.color("white") # Kolor
podpis.penup() # Narysuj sie
podpis.hideturtle() # Ukryj sie
podpis.goto(0, 260) # Pozycja
podpis.write("Kola by pansmutku", align="center", font=("Courier", 24, "normal") ) # Wystylizowany napis
# Caly czas odswiezaj okno

while True: # Petla nieskonczona
    okno.update() # Aktualizacja okna