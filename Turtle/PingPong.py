import turtle

wn = turtle.Screen() # Window  - okno nazwane wn
wn.title("Ping Pong by pansmutku") # Dodanie tytulu
wn.bgcolor("black") # Ustawienie tla na kolor czarny
wn.setup(width=800, height=600) # Ustawienie wymiarow
wn.tracer(0) # aktualizacje okna

# Score
score_a = 0
score_b = 0


# Paddle A - Paletka A - lewa strona
paddle_a = turtle.Turtle()
paddle_a.speed(0) # Szybkosc animacji, maksymalna
paddle_a.shape("square") # Ksztalt naszej paletki
paddle_a.color("white") # Kolor paletki
paddle_a.shapesize(stretch_wid=5, stretch_len=1)
paddle_a.penup()
paddle_a.goto(-350, 0) # Pozycja naszej paletki

# Paddle B - Paletka B - prawa strona
paddle_b = turtle.Turtle()
paddle_b.speed(0) # Szybkosc animacji, maksymalna
paddle_b.shape("square") # Ksztalt naszej paletki
paddle_b.color("white") # Kolor paletki
paddle_b.shapesize(stretch_wid=5, stretch_len=1)
paddle_b.penup()
paddle_b.goto(350, 0) # Pozycja naszej paletki


# Ball - Pilka
ball = turtle.Turtle()
ball.speed(0) # Szybkosc animacji, maksymalna
ball.shape("square") # Ksztalt naszej pileczki
ball.color("white") # Kolor pileczki
ball.penup()
ball.goto(0, 0) # Pozycja naszej pileczki
ball.dx = 0.1 # (def2)za kazdym razem kiedy sie porusza, zmienia pozycje o 2px
ball.dy = 0.1 # (def2)za kazdym razem kiedy sie porusza, zmienia pozycje o 2px

# Pen
pen = turtle.Turtle()
pen.speed(0) # Animation speed  / Szybkość animacji
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Player A: 0 Player B: 0", align="center", font=("Courier", 24, "normal") )

# Function - Funkcja
def paddle_a_up():
    y = paddle_a.ycor() # Pobieramy aktualna pozycje naszej paletki a przy uzyciu metody z turtle
    y += 20 # Dodajemy 20px do y
    paddle_a.sety(y) # Ustaw y paletki na nowe y

def paddle_a_down():
    y = paddle_a.ycor() # Pobieramy aktualna pozycje naszej paletki a przy uzyciu metody z turtle
    y -= 20 # Dodajemy 20px do y
    paddle_a.sety(y) # Ustaw y paletki na nowe y

def paddle_b_up():
    y = paddle_b.ycor() # Pobieramy aktualna pozycje naszej paletki a przy uzyciu metody z turtle
    y += 20 # Dodajemy 20px do y
    paddle_b.sety(y) # Ustaw y paletki na nowe y

def paddle_b_down():
    y = paddle_b.ycor() # Pobieramy aktualna pozycje naszej paletki a przy uzyciu metody z turtle
    y -= 20 # Dodajemy 20px do y
    paddle_b.sety(y) # Ustaw y paletki na nowe y

# Bez antyghostingu
# Keyboard binding - bindowanie przyciskow klawiatury
wn.listen()
wn.onkeypress(paddle_a_up, "w") # Po nacisnieciu w wywolaj paddle_a_up
wn.onkeypress(paddle_a_down, "s") # Po nacisnieciu s wywolaj paddle_a_down
wn.onkeypress(paddle_b_up, "Up") # Po nacisnieciu strzalki w gore wywolaj paddle_b_up
wn.onkeypress(paddle_b_down, "Down") # Po nacisnieciu strzalki w dol wywolaj paddle_b_down


# Main game loop - glowna petla gry
while True: # Petla while
    wn.update() # Aktualizacja ekranu kiedy tylko petla dziala

    # Move the ball - Sprawmy, aby pilka zaczela sie ruszac
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

    # Border checking
    # Gora
    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1 # Odbijamy (zmieniamy kierunek)
    # Dol
    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1 # Odbijamy (zmieniamy kierunek)
    # Prawa
    if ball.xcor() > 390:
        ball.goto(0, 0) # Wracamy na srodek
        ball.dx *= -1 # Zmieniamy kierunek
        score_a += 1 # Dodajemy punkt dla gracza a
        pen.clear() # "Czyscimy" napis
        pen.write("Player A: {} Player B: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))

    # Lewa
    if ball.xcor() < -390:
        ball.goto(0, 0) # Wracamy na srodek
        ball.dx *= -1 # Zmieniamy kierunek
        score_b += 1 # Dodajemy punkt dla gracza b
        pen.clear() # "Czyscimy" napis
        pen.write("Player A: {} Player B: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))


    # Paddle and ball collisions
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 40 and ball.ycor() > paddle_b.ycor() - 40):
        ball.setx(340)
        ball.dx *= -1

    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_a.ycor() + 40 and ball.ycor() > paddle_a.ycor() - 40):
        ball.setx(-340)
        ball.dx *= -1
# Podczas pisania trzeba byc konsekwentnym,
# uzywac TAB`ow lub SPACE - nie mieszac!

# https://www.youtube.com/watch?v=XGf2GcyHPhc
# Core do wiekszego projektu