import turtle # Importujemy turtle`a


# Okno
okno = turtle.Screen() # Okienko
okno.title("Kwadraty by pansmutku") # Tytul
okno.bgcolor("black") # Kolor tla
okno.setup(width=700, height=600) # Rozdzielczosc
okno.tracer(0) # Update

# Maly kwadrat
kwadratmaly = turtle.Turtle() # Definiujemy kwadratmaly
kwadratmaly.speed(0) # Szybkosc animacji
kwadratmaly.goto(-300, 0) # Pozycja
kwadratmaly.color("white") # Kolor
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.penup() # Narysuj sie

# Sredni kwadrat
kwadratsredni = turtle.Turtle() # Definiujemy kwadratsredni
kwadratsredni.speed(0) # Szybkosc animacji
kwadratsredni.goto(-200, 0) # Pozycja
kwadratsredni.color("white") # Kolor
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.penup() # Narysuj sie

# Duzy kwadrat
kwadrat = turtle.Turtle() # Definiujemy kwadrat
kwadrat.speed(0) # Szybkosc animacji
kwadrat.goto(-50, 0) # Pozycja
kwadrat.color("white") # Kolor
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.penup() # Narysuj sie

# Kolorowe kwadraty

# Maly kwadrat
kwadratmaly = turtle.Turtle() # Definiujemy kwadratmaly
kwadratmaly.speed(0) # Szybkosc animacji
kwadratmaly.goto(-300, -200) # Pozycja
kwadratmaly.color("yellow") # Kolor
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.left(90) # Skrec w lewo o 90 stopni
kwadratmaly.forward(50) # Wykonaj ruch o 50px
kwadratmaly.penup() # Narysuj sie

# Sredni kwadrat
kwadratsredni = turtle.Turtle() # Definiujemy kwadratsredni
kwadratsredni.speed(0) # Szybkosc animacji
kwadratsredni.goto(-200, -200) # Pozycja
kwadratsredni.color("blue") # Kolor
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.left(90) # Skrec w lewo o 90 stopni
kwadratsredni.forward(100) # Wykonaj ruch o 100px
kwadratsredni.penup() # Narysuj sie

# Duzy kwadrat
kwadrat = turtle.Turtle() # Definiujemy kwadrat
kwadrat.speed(0) # Szybkosc animacji
kwadrat.goto(-50, -200) # Pozycja
kwadrat.color("red") # Kolor
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.left(90) # Skrec w lewo o 90 stopni
kwadrat.forward(150) # Wykonaj ruch o 150px
kwadrat.penup() # Narysuj sie

# Podpis
podpis = turtle.Turtle() # Definiujemy podpis
podpis.speed(0) # Animation speed  / Szybkość animacji
podpis.color("white") # Kolor
podpis.penup() # Narysuj sie
podpis.hideturtle() # Ukryj sie
podpis.goto(0, 260) # Pozycja
podpis.write("Kwadraty by pansmutku", align="center", font=("Courier", 24, "normal") ) # Wystylizowany napis
# Caly czas odswiezaj okno

while True: # Petla nieskonczona
    okno.update() # Aktualizacja okna